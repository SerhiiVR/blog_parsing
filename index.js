const puppeteer = require('puppeteer');
const cheerio = require('cheerio');
const fs = require('fs');
var dateFormat = require("dateformat");
const moment = require('moment');
const pLimit = require('p-limit');
const { nextTick } = require('process');

const SITE = 'https://interns-blog.herokuapp.com';
const SITE_TOPIC = `${SITE}/v1/topic`;
const TOPIC_LIMIT = 10;
const LIMIT_SIMULTANEOUS_FILE_WRITE = 3;

const limit = pLimit(LIMIT_SIMULTANEOUS_FILE_WRITE);

(async () => {

    let topicUrlArray = await getTopicsUrl(SITE_TOPIC);

    for (let i = 0; i <= topicUrlArray.length; i++) {
        if (i >= TOPIC_LIMIT) break;
        limit(async () => {
            let postsArray = await getPosts(topicUrlArray[i]);
            let jsonObject = await createJsonObject(postsArray);
            writeToFile(jsonObject, i + 1)
        });
    }
})();


async function getTopicsUrl(siteUrl) {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto(siteUrl);
    let $ = cheerio.load(await page.content());
    await browser.close();

    let topicUrlArray = [];

    $('h2.post-title').each(function (i, element) {
        let topicUrl = $(element).find('a').attr('href');
        if (!topicUrlArray.includes(`${SITE}${topicUrl}`)) {
            topicUrlArray[i] = `${SITE}${topicUrl}`;
        }
    });

    topicUrlArray = topicUrlArray.filter(element => element !== null);

    return topicUrlArray;
}

async function getPosts(url) {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto(url);
    let $ = cheerio.load(await page.content());
    await browser.close();

    let postsArray = [];

    $('section.message').each(function (i, element) {
        let time = $(element).find('p.message-meta').find('span').text().split(' ');
        let postCreationDate = dateFormat(
            Date.now() - (Date.now() - moment().subtract(time[0], time[1])),
            'isoDate');
        let postAuthor = $(element).find('a.message-author').text();
        let postText = $(element).find('div.message-description').text();
        postsArray[i] = {
            author: postAuthor,
            date: postCreationDate,
            text: postText,
        }
    });

    return postsArray;
}

async function createJsonObject(topicArray) {
    let jsonObject = {};
    i = 1;
    topicArray.forEach(post => {
        jsonObject[`post${i}`] = post;
        i++;
    });

    return JSON.stringify(jsonObject);
}

async function writeToFile(object, postNumber) {
    fs.appendFileSync(`topic${postNumber}.json`,
        object,
        "UTF-8",
        { 'flags': 'a+' });
}